package app;

import java.util.Random;

public class App {

	public static void main(String[] args) {
		Random rand = new Random();
		for (int i = 0; i < 1000; i++) {
			int year = 1900 + rand.nextInt(100);
			int month = rand.nextInt(13);
			int day = rand.nextInt(29);
			int n1 = 9;
			int n2 = 8;
			int n3 = 7;
			int y3 = (year / 10) - (year / 100 * 10);
			int y4 = year - (year / 10 * 10);
			int m1 = month / 10;
			int m2 = month - m1 * 10;
			int d1 = day / 10;
			int d2 = day - d1 * 10;

			int check = mul(y3) + y4 + mul(m1) + m2 + mul(d1) + d2 + mul(n1) + n2 + mul(n3);
			check = check - (check / 10 * 10);
			check = 10 - check;
			if (check == 10) {
				check = 0;
			}

			System.out.print(year);
			System.out.print(month > 9 ? month : "0" + month);
			System.out.print(day > 9 ? day : "0" + day);
			System.out.print(n1);
			System.out.print(n2);
			System.out.print(n3);
			System.out.print(check);
			System.out.println();
		}
	}

	static int mul(int n) {
		n = n * 2;
		while (n > 9) {
			n = (n / 10) + (n - n / 10 * 10);
		}
		return n;
	}

}
